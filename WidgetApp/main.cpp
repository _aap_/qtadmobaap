#include "MainWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  // MainWindow w;
  // w.showMaximized();
  QScopedPointer<MainWindow> w;
  w.reset(new MainWindow());
  w->show();
  return a.exec();
}
